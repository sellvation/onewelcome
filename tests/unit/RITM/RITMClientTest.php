<?php
declare(strict_types=1);

namespace unit\RITM;

use Assert\AssertionFailedException;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Sellvation\OneWelcome\APIClient;
use Sellvation\OneWelcome\Credentials;
use Sellvation\OneWelcome\Exceptions\APIException;
use Sellvation\OneWelcome\RITM\RITMClient;
use JsonException;

class RITMClientTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $mockResponse;

    /**
     * @var MockObject
     */
    private $mockStream;

    /**
     * @var RITMClient
     */
    private $ritmAPI;

    /**
     * @phpstan-ignore-next-line
     * @throws AssertionFailedException
     */
    protected function setUp(): void
    {
        $this->mockResponse = $this->createMock(Response::class);
        $this->mockStream = $this->createMock(Stream::class);
        $mockClient = $this->createMock(GuzzleClient::class);

        $this->mockResponse->method('getBody')->willReturn($this->mockStream);
        $mockClient->method('request')->willReturn($this->mockResponse);

        $client = new APIClient($mockClient);

        $credentials = Credentials::fromOneWelcomeAPIResponse([
            'access_token' => '123',
            'refresh_token' => '456',
            'scope' => 'scope',
            'id_token' => '789',
            'token_type' => 'foo',
            'expires_in' => 2000,
        ]);

        $this->ritmAPI = new RITMClient($client, $credentials);
    }

    /**
     * @phpstan-ignore-next-line
     * @throws APIException|JsonException|AssertionFailedException
     */
    public function testShouldReturnCorrectValuesWhenCreatingNewUserObject(): void
    {
        $responseJson = '{"totalItems":1,"limit":10,"page":1,"pageCount":1,"result":[{"profileInformation":{"uid":"17e44a10-af8d-1ff1-1c11-1f911a34b1a3","name":{"familyName":"Smith","givenName":"John"},"addresses":[{"country":"NL","type":"home","iwelcome_street":"Street","iwelcome_houseNumber":"1","iwelcome_additional":"-","iwelcome_zone":"1000AA","iwelcome_area":"Amsterdam"}],"emails":[{"value":"email@domain.com","type":"home"}],"phoneNumbers":[{"value":"+316123456789","type":"other"}],"urn:scim:schemas:extension:iwelcome:1.0:ritm":[],"'. getenv('RITM_CUSTOMER_TAG') .'":{"'. getenv('RITM_CUSTOMER_KEY') .'":"123456789"},"urn:scim:schemas:extension:iwelcome:1.0":{"state":"ACTIVE"}},"roleAssignments":{"adminRoles":[],"personalRoles":[],"accessRoles":[]},"structureMemberships":[{"code":"structure-gaqdh41UWgh2","name":"Name","groupMemberships":[{"code":"17e41a71-af1d-1ff1-1c11-1191e134bfa1","name":"47144a10-a18d-41f1-1c11-1f91ea14bf13","attributes":{"owner":"41e41a71-af1d-41f1-b111-1191e134b1a3"}}]}]}]}';
        $responseArray = json_decode($responseJson, true, 512, JSON_THROW_ON_ERROR);

        $this->mockResponse->method('getStatusCode')->willReturn(200);
        $this->mockStream->method('__toString')->willReturn($responseJson);

        $userArray = $responseArray;
        $userObject = $this->ritmAPI->getUserByUUID('123456789');

        $profileInformation = $userArray['result'][0]['profileInformation'];

        $this->assertEquals($profileInformation['uid'], $userObject->getUUID());
        $this->assertEquals($profileInformation[getenv('RITM_CUSTOMER_TAG')][getenv('RITM_CUSTOMER_KEY')], $userObject->getCustomerId());
        $this->assertEquals($profileInformation['name']['givenName'], $userObject->getFirstName());
        $this->assertEquals($profileInformation['name']['familyName'], $userObject->getLastName());
        $this->assertEquals($profileInformation['urn:scim:schemas:extension:iwelcome:1.0']['state'], $userObject->getState());

        $this->assertCount(1, $userObject->getEmails());
        $this->assertEquals($profileInformation['emails'][0]['value'], $userObject->getEmails()->current()->getValue());
        $this->assertEquals($profileInformation['emails'][0]['type'], $userObject->getEmails()->current()->getType());
    }
}
